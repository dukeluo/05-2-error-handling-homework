package com.twuc.webApp.web;

import com.twuc.webApp.WebAppApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class MazeExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(WebAppApplication.class);


    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleException(RuntimeException ex, WebRequest request) {

        logger.error(ex.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(ex.getMessage());
    }

}
