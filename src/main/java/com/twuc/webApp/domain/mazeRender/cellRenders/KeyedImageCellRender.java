package com.twuc.webApp.domain.mazeRender.cellRenders;

import com.twuc.webApp.domain.mazeRender.CellRender;
import com.twuc.webApp.domain.mazeRender.RenderCell;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.HashMap;

/**
 * 该渲染器使用指定索引上的图像渲染指定的迷宫网格。
 */
public abstract class KeyedImageCellRender implements CellRender {
    private final HashMap<String, BufferedImage> map;

    /**
     * 创建一个 {@link KeyedImageCellRender} 实例。
     *
     * @param map 图像以及图像索引。
     */
    protected KeyedImageCellRender(HashMap<String, BufferedImage> map) {
        this.map = map;
    }

    /**
     * 当前渲染器是否支持渲染指定的迷宫网格。
     *
     * @param cell 迷宫网格。
     * @return 如果支持则返回 {@code true} 否则返回 {@code false}。
     */
    protected abstract boolean isSupported(RenderCell cell);

    /**
     * 根据当前网格的信息获得渲染图像的索引。
     *
     * @param cell 迷宫网格
     * @return 渲染图像的索引。
     */
    protected abstract String getTextureKey(RenderCell cell);

    @Override
    public void render(Graphics context, Rectangle cellArea, RenderCell cell) {
        if (!isSupported(cell)) return;

        String textureKey = getTextureKey(cell);
        BufferedImage texture = map.get(textureKey);
        if (texture == null) return;

        int x = ((cellArea.x * 2 + cellArea.width) / 2) - texture.getWidth() / 2;
        int y = (cellArea.y + cellArea.height) - texture.getHeight();

        context.drawImage(texture, x, y, null);
    }
}
