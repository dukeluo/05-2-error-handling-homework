package com.twuc.webApp.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
public class MazeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_get_400_when_the_type_is_wrong() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/mazes/black"))
                .andExpect(MockMvcResultMatchers.status().is(400))
                .andExpect(MockMvcResultMatchers.content().string("Invalid type: black"));
    }

}

//@SpringBootTest (webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@AutoConfigureWebClient
//public class MazeControllerTest {
//
//    @Autowired
//    private TestRestTemplate testRestTemplate;
//
//    @Test
//    void should_get_400_when_the_type_is_wrong() throws Exception {
//        ResponseEntity<String> entity = testRestTemplate.getForEntity("/mazes/black", String.class);
//
//        assertEquals(400, entity.getStatusCodeValue());
//        assertEquals("Invalid type: black", entity.getBody());
//    }
//
//}

